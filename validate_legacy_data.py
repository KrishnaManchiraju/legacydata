import argparse
import ast
import json
import math
import os
import pandas as pd
import re
import phonenumbers

from datetime import datetime
from pathlib import Path

VALID_STATUSES = ["Received", "Registered", "Cancelled", "Rejected"]

VALID_PROGS = [
    "None",
    "Isha Yoga",
    "Inner Engineering",
    "BSP",
    "Samyama",
    "ShoonyaIntensive",
    "Wholeness",
]

DATE_PATTERNS = [
    "%b-%y",
    "%b-%Y",
    "%B-%y",
    "%B-%Y",
    "%b/%y",
    "%b/%Y",
    "%B/%y",
    "%B/%Y",
    "%b%y",
    "%b%Y",
    "%B%y",
    "%B%Y",
    "%b.%y",
    "%b.%Y",
    "%B.%y",
    "%B.%Y",
    "%b %y",
    "%b %Y",
    "%B %y",
    "%B %Y",
    "%m-%y",
    "%m-%Y",
    "%M-%y",
    "%M-%Y",
    "%m/%y",
    "%m/%Y",
    "%M/%y",
    "%M/%Y",
]

empty_records = {
    "sector": [],
    "meta": [],
    "group": [],
    "state": [],
    "country": [],
    "nationality": [],
    "name": [],
    "firstname": [],
    "lastname": [],
    "dob": [],
    "gender": [],
    "email": [],
    "ishaprograms": [],
    "ishayoga_date": [],
    "ie_date": [],
    "wholeness_date": [],
    "ieo_date": [],
    "shambhavi_date": [],
    "bsp_date": [],
    "samyama_date": [],
    "yatra": [],
    "acn": [],
}

invalid_recs = {
    "invalid_names": {"firstname": [], "lastname": [], "name": []},
    "invalid_dobs": [],
    "invalid_emails": [],
    "invalid_progs": [],
    "invalid_prog_date": [],
}

all_coountry_details = pd.read_excel("./AllCountryCodes.xlsx")
known_countries = all_coountry_details["COUNTRY"].tolist()
known_iso2_codes = all_coountry_details["ISO"].tolist()


def is_not_null(s_no: int, val: str, col_name: str) -> bool:
    if not val:
        global record_valid_g, empty_records, filtered_data
        record_valid_g = False
        empty_records[col_name].append(s_no)
        filtered_data.at[s_no, "validation_status"] = (
            col_name + " missing, " + filtered_data.at[s_no, "validation_status"]
        )

        return False
    return True


def validate_name(s_no: int, name: str, col_name: str) -> bool:
    if not bool(re.fullmatch("[A-zÀ-ž-.' ]{2,}", name)):
        global record_valid_g, invalid_recs, filtered_data
        record_valid_g = False
        invalid_recs["invalid_names"][col_name].append(s_no)
        filtered_data.at[s_no, "validation_status"] = (
            col_name + " invalid, " + filtered_data.at[s_no, "validation_status"]
        )

        return False
    return True


def validate_date(s_no: int, date_text: str) -> bool:
    try:
        datetime.strptime(date_text.split("T")[0], "%Y-%m-%d")

        return True
    except ValueError:
        global record_valid_g, invalid_recs, filtered_data
        record_valid_g = False
        invalid_recs["invalid_dobs"].append(s_no)
        filtered_data.at[s_no, "validation_status"] = (
            "DOB invalid, " + filtered_data.at[s_no, "validation_status"]
        )

        return False


def guess_date(s_date: str):
    cap_date = s_date.capitalize()
    for pattern in DATE_PATTERNS:
        try:
            return datetime.strptime(cap_date, pattern).date()
        except:
            pass
    return False


def validate_prog_date(s_no: int, date_text: str, prog: str) -> bool:
    global filtered_data

    prog_date = guess_date(date_text)

    if prog_date:
        filtered_data.at[s_no, prog] = prog_date.strftime("%b-%Y")

        return True
    else:
        return False


def validate_email(s_no: int, email: str) -> bool:
    if re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return True
    else:
        global record_valid_g, invalid_recs, filtered_data
        record_valid_g = False
        invalid_recs["invalid_emails"].append(s_no)
        filtered_data.at[s_no, "validation_status"] = (
            "email invalid, " + filtered_data.at[s_no, "validation_status"]
        )

        return False


def validate_programs(s_no: int, progs) -> bool:
    if set(progs).issubset(VALID_PROGS):
        return True
    else:
        global record_valid_g, invalid_recs, filtered_data
        record_valid_g = False
        invalid_recs["invalid_progs"].append(s_no)
        filtered_data.at[s_no, "validation_status"] = (
            "programs invalid, " + filtered_data.at[s_no, "validation_status"]
        )

def get_national_number(num: str, country_alpha: str) -> str:
    num_c = num.replace("+", "").replace("-", "").replace(" ", "")
    try:
        parsed_ph = phonenumbers.parse(num_c, country_alpha)
        return parsed_ph.national_number
    except phonenumbers.NumberParseException:
        return num_c

def clean_ph_num(s_no: int, mobile: str, phone: str, country: str):
    global filtered_data
    c_index = known_countries.index(country.strip())
    country_alpha_code = known_iso2_codes[c_index]
    
    if mobile:
        filtered_data.at[s_no, "mobile"] = get_national_number(mobile, country_alpha_code)
    if phone:
        filtered_data.at[s_no, "phone"] = get_national_number(phone, country_alpha_code)


# Print iterations progress
def printProgressBar(
    iteration,
    total,
    prefix="Progress:",
    suffix="Complete",
    decimals=1,
    length=50,
    fill="█",
    printEnd="\r",
):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + "-" * (length - filledLength)
    print(f"\r{prefix} |{bar}| {percent}% {suffix}", end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


def just_validate():
    """
    FIELD: VALIDATORS
    sector: not_null
    meta.status: not_null, known_status
    group: not_null
    state: not_null
    country: not_null
    nationality: not_null
    name: not_null if any of (firstname or lastname) is null, valid_name
    firstname
    lastname
    dob: not_null, valid_date (%Y-%m-%d format)
    gender: not_null
    email: not_null, valid_email
    ishaprograms -> in known programs (also used for reference)
    ishayoga_date: not_null if ishaprograms has "Isha Yoga"
    ie_date: not_null if ishaprograms has "Inner Engineering"
    wholeness_date: not_null if ishaprograms has "Wholeness"
    ieo_date: not_null if ishaprograms has "IE Online"
    shambhavi_date: not_null if ishaprograms has "Shambhavi Initiation"
    bsp_date: not_null if ishaprograms has "BSP"
    samyama_date: not_null if ishaprograms has "Samyama"
    yatra: not_null
    acn: not_null
    """
    invalid_recs = 0
    global filtered_data
    tot_recs = filtered_data.shape[0]
    local_i = 1

    for index, row in filtered_data.iterrows():
        global record_valid_g
        record_valid_g = True
        is_not_null(index, row["sector"], "sector")
        is_not_null(index, row["meta.status"], "meta.status")
        is_not_null(index, row["group"], "group")
        is_not_null(index, row["country"], "country")
        is_not_null(index, row["nationality"], "nationality")

        if row["country"] == "India":
            is_not_null(index, row["state"], "state")

        if row["firstname"] and row["lastname"]:
            validate_name(index, row["firstname"], "firstname")
            validate_name(index, row["lastname"], "lastname")
        else:
            is_not_null(index, row["name"], "name")
            validate_name(index, row["name"], "name")

        if is_not_null(index, row["dob"], "dob"):
            validate_date(index, row["dob"])

        is_not_null(index, row["gender"], "gender")

        if is_not_null(index, row["email"], "email"):
            validate_email(index, row["email"])

        clean_ph_num(index, row["mobile"], row["phone"], row["country"])

        isha_programs = ast.literal_eval(row["ishaprograms"])
        # validate_programs(index, isha_programs)

        if "Isha Yoga" in isha_programs:
            if row["ishayoga_date"] and validate_prog_date(index, row["ishayoga_date"], "ishayoga_date"):
                pass
            else:
                row["ishayoga_date"] = ""
                isha_programs.remove("Isha Yoga")
        if "Inner Engineering" in isha_programs:
            if row["ie_date"] and validate_prog_date(index, row["ie_date"], "ie_date"):
                pass
            else:
                row["ie_date"] = ""
                isha_programs.remove("Inner Engineering")
        if "Wholeness" in isha_programs:
            if row["wholeness_date"] and validate_prog_date(index, row["wholeness_date"], "wholeness_date"):
                pass
            else:
                row["wholeness_date"] = ""
                isha_programs.remove("Wholeness")
        if "IE Online" in isha_programs:
            if row["ieo_date"] and validate_prog_date(index, row["ieo_date"], "ieo_date"):
                pass
            else:
                row["ieo_date"] = ""
                isha_programs.remove("IE Online")
        if "Shambhavi Initiation" in isha_programs:
            if row["shambhavi_date"] and validate_prog_date(index, row["shambhavi_date"], "shambhavi_date"):
                pass
            else:
                row["shambhavi_date"] = ""
                isha_programs.remove("Shambhavi Initiation")
        if "BSP" in isha_programs:
            if row["bsp_date"] and validate_prog_date(index, row["bsp_date"], "bsp_date"):
                pass
            else:
                row["bsp_date"] = ""
                isha_programs.remove("BSP")
        if "Samyama" in isha_programs:
            if row["samyama_date"] and validate_prog_date(index, row["samyama_date"], "samyama_date"):
                pass
            else:
                row["samyama_date"] = ""
                isha_programs.remove("Samyama")

        row["ishaprograms"] = '['+','.join(['"{}"'.format(value) for value in isha_programs])+']'

        is_not_null(index, row["yatra"], "yatra")
        is_not_null(index, row["acn"], "acn")

        if not record_valid_g:
            filtered_data.at[index, "Valid"] = False
            invalid_recs = invalid_recs + 1
        printProgressBar(local_i, tot_recs)
        local_i = local_i + 1

    print("\n\nInvalid records: " + str(invalid_recs))


def main():
    parser = argparse.ArgumentParser(description="Validates legacy data")
    parser.add_argument(
        "file",
        type=str,
        help="file name (including full path)",
    )
    parser.add_argument(
        "-o",
        "--output_path",
        help="Optionally it it's possible to specify the desired location for the output files",
    )

    args = parser.parse_args()

    op_path = Path(".")
    if args.output_path:
        op_path = Path(args.output_path).resolve()
    if not os.path.exists(op_path):
        os.makedirs(op_path)

    data = pd.read_csv(args.file).replace(math.nan, "")
    data = data.applymap(str)

    global filtered_data
    status_filter = data["meta.status"].isin(VALID_STATUSES)
    filtered_data = data[status_filter].copy()

    filtered_data.insert(loc=0, column="s_no", value=filtered_data.index)
    filtered_data.insert(loc=1, column="validation_status", value="")
    filtered_data.insert(loc=2, column="Valid", value=True)

    just_validate()
    actual_file_name = os.path.basename(args.file)
    filtered_file_name = op_path / ("filtered_" + actual_file_name)
    log_file_name = op_path / ("filtered_" + actual_file_name.split(".")[0] + ".log")

    # filtered_data = filtered_data.replace(False, "")
    filtered_data["Valid"] = filtered_data["Valid"].replace("", False)
    filtered_data.to_csv(filtered_file_name, index=False)

    f = open(log_file_name, "w")
    f.write(json.dumps(empty_records, sort_keys=True, indent=4))
    f.write(json.dumps(invalid_recs, sort_keys=True, indent=4))
    f.close()

    print("Total records: " + str(data.shape[0]))
    print("Filtered records: " + str(filtered_data.shape[0]))
    print(f"\nWritten {filtered_file_name} and {log_file_name} files")


if __name__ == "__main__":
    main()