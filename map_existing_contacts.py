import argparse
import math
import pandas as pd
import os

from timeit import default_timer as timer
from datetime import datetime
from pathlib import Path

parser = argparse.ArgumentParser(description="Maps Exisitng CRM Conacts")
parser.add_argument(
    "file",
    type=str,
    help="file name (including full path)",
)

parser.add_argument(
    "input",
    type=str,
    help="CRM contacts export as input. Only csv file is supported",
)

parser.add_argument(
    "-o",
    "--output_path",
    help="Optionally it it's possible to specify the desired location for the output files",
)

args = parser.parse_args()

op_path = Path(".")
if args.output_path:
    op_path = Path(args.output_path).resolve()
if not os.path.exists(op_path):
    os.makedirs(op_path)

all_crm_contacts = pd.read_csv(
    args.input,
    usecols=[
        "ID",
        "First Name",
        "Last Name",
        "Email Address",
        "Non Primary E-mails",
        "Mobile",
        "Home",
        "Other Phone",
        "Birthdate",
    ],
    dtype={
        "ID": "str",
        "First Name": "str",
        "Last Name": "str",
        "Email Address": "str",
        "Non Primary E-mails": "str",
        "Mobile": "str",
        "Home": "str",
        "Other Phone": "str",
        "Birthdate": "str",
    },
).replace(math.nan, "")
all_crm_contacts = all_crm_contacts.applymap(str)
all_crm_contacts["Mobile"] = all_crm_contacts["Mobile"].apply(
    lambda x: x if len(x) > 5 else ""
)
all_crm_contacts["Home"] = all_crm_contacts["Home"].apply(
    lambda x: x if len(x) > 5 else ""
)
all_crm_contacts["Other Phone"] = all_crm_contacts["Other Phone"].apply(
    lambda x: x if len(x) > 5 else ""
)
all_crm_contacts["Email Address"] = all_crm_contacts["Email Address"].apply(
    lambda x: x.lower()
)
all_crm_contacts["Non Primary E-mails"] = all_crm_contacts["Non Primary E-mails"].apply(
    lambda x: x.lower()
)
print("CRM contacts loaded")


legacy_contacts = pd.read_csv(args.file,  dtype=str).replace(math.nan, "")
legacy_contacts = legacy_contacts.applymap(str)
legacy_contacts.insert(loc=0, column="MatchStatus", value="")
legacy_contacts["email"] = legacy_contacts["email"].apply(
    lambda x: x.lower()
)
print("Legacy contacts loaded")


def is_email_matching(legacy_email: str, crm_email1: str, crm_email2: str) -> bool:
    legacy_email = legacy_email.strip().lower()
    crm_email1 = crm_email1.strip().lower()
    crm_email2 = crm_email2.strip().lower()
    
    if legacy_email and (legacy_email == crm_email1 or legacy_email == crm_email2):
        return True
    else:
        return False


def is_phone_matching(legacy_phone: str, row) -> bool:
    legacy_phone = legacy_phone
    crm_mobile = row["Mobile"]
    crm_home_phone = row["Home"]
    crm_Other_phone = row["Other Phone"]

    if crm_mobile and (legacy_phone in crm_mobile):
        return True

    if crm_home_phone and (legacy_phone in crm_home_phone):
        return True

    if crm_Other_phone and (legacy_phone in crm_Other_phone):
        return True

    return False


def is_dob_matching(dob: str, crm_dob: str):
    try:
        legacy_dob = datetime.strptime(dob.split("T")[0], "%Y-%m-%d")
        crm_dob = datetime.strptime(crm_dob, "%d/%m/%Y")

        return legacy_dob == crm_dob
    except Exception:
        return False

def is_name_matching(legacy_name: str, crm_first_name: str, crm_last_name: str) -> bool:
    if legacy_name and (crm_last_name or crm_first_name):
        legacy_name_ = legacy_name.lower()
        crm_name_ = (crm_first_name + " " + crm_last_name).lower()
        if (legacy_name_ in crm_name_) or (crm_name_ in legacy_name_):
            return True
    return False

# Print iterations progress
def printProgressBar(
    iteration,
    total,
    prefix="Progress:",
    suffix="Complete",
    decimals=1,
    length=50,
    fill="█",
    printEnd="\r",
):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + "-" * (length - filledLength)
    print(f"\r{prefix} |{bar}| {percent}% {suffix}", end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


tot_recs = legacy_contacts.shape[0]
for index, row in legacy_contacts.iterrows():
    for crm_index, crm_row in all_crm_contacts.iterrows():
        email_match = is_email_matching(
            row["email"], crm_row["Email Address"], crm_row["Non Primary E-mails"]
        )
        # print(f"email: {row['email']} email_match: {email_match}")
        phone_match = (
            is_phone_matching(row["phone"], crm_row) if row["phone"] else False
        )
        mobile_match = (
            is_phone_matching(row["mobile"], crm_row) if row["mobile"] else False
        )
        phone_match = phone_match or mobile_match
        
        dob_match = False
        if email_match or phone_match:
            dob_match = is_dob_matching(row["dob"], crm_row["Birthdate"])

        if email_match and phone_match and dob_match:
            row["MatchStatus"] = "FullMatch"
            break
        elif email_match and phone_match:
            if is_name_matching(row["name"], crm_row["First Name"], crm_row["Last Name"]):
                row["dob"] = datetime.strftime(crm_row["Birthdate"],"%Y-%m-%dT00:00:00.000Z")
                row["MatchStatus"] = "FullMatch"
                break
            row["MatchStatus"] = "Email_Phone_Match"
        elif email_match and dob_match:
            row["MatchStatus"] = "FullMatch"
            break
        elif phone_match and dob_match:
            if is_name_matching(row["name"], crm_row["First Name"], crm_row["Last Name"]):
                row["email"] = crm_row["Email Address"]
                row["MatchStatus"] = "FullMatch"
                break
            row["MatchStatus"] = "Phone_DOB_Match"
            row["validation_status"] = crm_row["ID"]
            break
        elif email_match:
            row["MatchStatus"] = "EmailMatch"
        elif phone_match:
            row["MatchStatus"] = "PhoneMatch"
    printProgressBar(index + 1, tot_recs)


actual_file_name = os.path.basename(args.file)
op_file_name = op_path / ("mapped_" + actual_file_name)
legacy_contacts.to_csv(op_file_name, index=False)
print(f"{op_file_name} file created")