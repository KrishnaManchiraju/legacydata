## To clone: 
git clone

## To update: 
git pull

## Create Virtual Environment:
python -m venv env

## Enter Virtual Environment:

### On macOS and Linux:
source env/bin/activate

### On Windows:
.\env\Scripts\activate

## Install dependencies:
pip install -r requirements.txt


## First stage of cleaning
python validate_legacy_data.py <file path>
eg:
python validate_legacy_data.py /home/krishna/Downloads/ishalivedata_09Nov2020/yatra_KM17_copy.csv

Note: in windows you may have to use \ instead of /

This will create an output file: filtered_<given file name> eg: filtered_yatra_KM17_copy.csv

## Second Stage:
Map the legacy data with existing CRM contacts

1. Export the contact data from CRM and save it as CRM_Contacts.csv in same location as the scripts are
2. Execute python map_existing_contacts.py <file path> (use the filtered data from the previous step)
   eg: python map_existing_contacts.py ./filtered_yatra_KM17_copy.csv
   Note: This step takes long time to execute.
3. Leave the step2 executing and go to Dhyanalinga :p

This will create mapped_yatra_KM17_copy.csv